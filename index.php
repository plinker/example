<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

date_default_timezone_set("Europe/London");

require "vendor/autoload.php";

/**
 * Server Listener
 */
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $server = new \Plinker\Core\Server(
        $_POST,
        "",
        ""
    );

    exit($server->execute());
}

exit(header("HTTP/1.1 204 No Content", 204, true));
