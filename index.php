<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

date_default_timezone_set('Europe/London');

require 'vendor/autoload.php';

/**
 * POST Server Part
 */
if ($_SERVER['REQUEST_METHOD'] == 'POST' && !isset($_POST['the_key'])) {

    $server = new plinker\Core\Server(
        $_POST,
        'username',
        'password'
    );

    exit($server->execute());
}

//exit(header("HTTP/1.1 204 No Content", 204, true));

/**
 * Client examples - dev stuff really
 */

/**
 * Initialize client connection to host.
 *
 * @param string $url to host
 * @param string $component class to interface to
 * @param string $public_key to authenticate on host
 * @param string $private_key to authenticate on host
 */
$plink = new plinker\Core\Client(
    'https://plinker-lcherone.c9.io',
    'Cron\Cron',
    'username',
    'password',
    array(
        'taskfile' => './cron-task-file'
    )
);

//echo '<pre>'.print_r($plink->read(''), true).'</pre>';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    foreach ($_POST as $key => $value) {
        $plink->create($key, $value);
    }
}
?>

<h1>Example - crontab</h1>

<pre><?= json_decode($plink->crontab()); ?></pre>


<form method="POST" action="">
    <p><textarea rows="10" name="the_key" cols="55"><?php echo json_decode($plink->read('the_key')); ?></textarea></p>
    <p><input type="submit" value="Submit"></p>
</form>









<?php return; ?>
// echo '<pre>'.print_r($plink->create('test', array(
//     'col1'=>'a',
//     'col2'=>'b',
//     'col3'=>'c',
// )), true).'</pre>';

// // example - use AsteriskManager component and call sipPeers
// $AsteriskManager = $link->useComponent('AsteriskManager');
// $result['AsteriskManager'] = $AsteriskManager->sipPeers();

// // example - use System component and call get_memory_stats
// $system = $link->useComponent('System');
// //echo '<pre>'.print_r($system, true).'</pre>';
// $result['memstats'] = $system->get_memory_stats('/');

// // example - use RedBean component and call findAll
// $redbean = $link->useComponent('RedBean');
// //echo '<pre>'.print_r($redbean, true).'</pre>';
// $result['redbean'] = $redbean->findAll(
//     'cdr',
//     ' id = ? ',
//     array(1)
// );
// $result['redbean'] = $redbean->inspect('cdr');
// $result['redbean'] = $redbean->inspect();

// // // example - use Base91 component and call encode on remote host
// $Base91 = $link->useComponent('Base91');

// $enc = json_decode($Base91->encode(
//     'Hello World'
// ));

// $result['Base91'] = array(
//     'encoded' => $enc,
//     'decoded' => $Base91->decode($enc)
// );


// echo '<pre>'.print_r($result, true).'</pre>';
